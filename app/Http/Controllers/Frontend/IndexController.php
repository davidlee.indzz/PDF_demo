<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

// Include the main TCPDF library (search for installation path).
require_once(base_path() . '/vendor/tecnickcom/tcpdf/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends \TCPDF {
    //Page header
    public function Header() {
        // Logo
        $image_file = '../public/images/'.'logo.png';
        $this->Image($image_file, 10, 10, 30, '', 'PNG', '', 'T', false, 600, '', false, false, 0, false, false, false);
        // Set font
        //$this->SetFont('helvetica', 'B', 20);
        \TCPDF_FONTS::addTTFfont('../resources/fonts/pingfangb.ttf', '', '', 32);
        $this->SetFont('pingfangb','', 14);

        // Title
        $this->MultiCell('', 5, '同成工程有限公司 Talent Engineering HK Ltd.', 0, '', 0, 1, 42, '', true);
        $this->SetFont('pingfangr','', 10);
        $this->MultiCell('', 5, 'Tel: 29509996 Fax : 30073431', 0, '', 0, 1, 42, '', true);
        $this->MultiCell('', 5, 'Website: www.talent.hk / Email: sales@talent.hk', 0, '', 0, 1, 42, '', true);

        // Add line
        $style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(100, 100, 100));
        $this->Line(10, 30, 200, 30, $style);

        // Details
        $this->MultiCell('', 5, 'Dr Vio & Partnes Ltd', 0, 'C', 0, 1, '', 35, true);

        $left_column = 'Tel:<br />'.'E-Mail:<br />'.'Attn.:';
        $left_column2 = '3183 9917<br />'.'helen.lam@drvio.com.hk<br />'.'Ms Helen Lam';
        $right_column = 'Fax:<br />'.'Address:';
        $right_column2 = '2367 6682<br />'.'上環干諾道西21-24號海景商業大廈3樓全層';

        // get current vertical position
        $y = $this->getY();

        // set color for background
        $this->SetFillColor(255, 255, 255);

        // set color for text
        $this->SetTextColor(0, 0, 0);

        $this->writeHTMLCell(20, '', 10, $y, $left_column, 0, 0, 1, true, 'J', true);
        $this->writeHTMLCell(56, '', 30, $y, $left_column2, 0, 0, 1, true, 'J', true);
        $this->writeHTMLCell(25, '', '', '', $right_column, 0, 0, 1, true, 'J', true);
        $this->writeHTMLCell(90, '', '', '', $right_column2, 0, 1, 1, true, 'J', true);

        // Add line
        $style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(100, 100, 100));
        $this->Line(10, 60, 200, 60, $style);

        // Quotation
        $this->SetFont('pingfangb','', 18);
        $this->SetTextColor(100,100,100);
        $this->MultiCell('', 5, 'Quotation', 0, 'C', 0, 1, '', 65, true);
        $this->SetFont('pingfangr','', 10);

        // Quotation details
        $left_column = 'Our Reference:<br />'.'Salesman:';
        $left_column2 = 'TEC103148<br />'.'Franky Fok';
        $right_column = 'Date:';
        $right_column2 = '2016-03-18';
        $y = $this->getY();
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0, 0, 0);
        $this->writeHTMLCell(30, '', 10, $y, $left_column, 0, 0, 1, true, 'J', true);
        $this->writeHTMLCell(90, '', 50, $y, $left_column2, 0, 0, 1, true, 'J', true);
        $this->writeHTMLCell(20, '', '', '', $right_column, 0, 0, 1, true, 'J', true);
        $this->writeHTMLCell(30, '', '', '', $right_column2, 0, 1, 1, true, 'J', true);
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->MultiCell(60, 5, 'www.talent.hk', 0, 'L', 0, 0, '', '', true);
        $this->MultiCell(60, 5, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, 'C', 0, 0, '', '', true);
        $this->MultiCell(60, 5, 'Version: 2016-03-18 15:59', 0, 'R', 0, 1, '', '', true);
    }
}

class IndexController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Index Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles index page
    |
    */
    public function index() {
        // create new PDF document
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Nicola Asuni');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(10, 85, 10);//PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set font
        //$pdf->SetFont('times', 'BI', 12);
        $pdf->SetFont('pingfangr','', 10);

        // add a page
        $pdf->AddPage();

        // define some HTML content with style
        $html = <<<EOF
        <style>
            .subtitle {
                font-size: 12pt;
            }
            table tr th {
                border: 1px solid #888888;
                background-color: #ececec;
            }
            table td {
                border: 1px solid #888888;
            }
            table td.highlight {
                background-color: #ececec;
            }
        </style>

        <table cellpadding="6">
                <tr>
                    <th width="48%">Item 項目</th>
                    <th width="15%" align="center">Quantity 數量</th>
                    <th width="20%" align="center">Unit Price 單價</th>
                    <th width="17%" align="right">Total 總價</th>
                </tr>
                <tr>
                    <td>Ref: TEC101512M15_R1<br /><br />
                    全年冷氣系統保養服務<br />
                    -<br />
                    就以下地址冷氣機系統提供為期十二個月的冷氣保養服務<br />
                    - 提供所需的工具及人力做服務<br />
                    - 全年免費冷氣機檢查服務(維修及零件另計)<br />
                    - 如有緊急情況，24小時內到場處理<br /><br />
                    基本資料：<br />
                    - 12部天花機，2部風喉機，7部掛牆機<br />
                    - 三菱電機掛牆式分體機︰MSD-CB09VD - 6部，MSD-CB12VD - 1部<br />
                    - 三菱電機藏天花卡式分體機︰SL-1.6AAK -1部，PL-3AAK - 11部<br />
                    - 三菱電機天花駁風喉分體機︰PE-3RAK - 1部，PU-P71VEA-SH - 1部<br /><br />
                    - 保養工程地址:<br />
                    上環干諾道西21-24號海景商業大廈3樓全層</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                    服務提供：<br />
                    1)    每年四次全年保養清洗隔塵網,冷氣機維護及報告<br />
                    - 約每3個月一次清洗隔塵網<br />
                    - 檢查出風量及排水<br />
                    - 檢查送風機摩打、壓縮機及散熱扇運作情況<br />
                    - 控需要檢查及調整控制裝置<br />
                    - 記錄任何冷氣系統失效及維護事故<br />
                    - 約每3個月一次冷氣維護及報告
                    </td>
                    <td align="center">4</td>
                    <td align="center">HK$1,890</td>
                    <td align="right">HK$7,560</td>
                </tr>
                <tr>
                    <td>
                    2) 每年1次大清洗全場冷氣機(10月-3月其中1次保養內同時進行)<br />
                    - 藥水清洗豬籠風扇一次<br />
                    - 清洗隔塵網<br />
                    - 吸去水及通去水<br />
                    - 清理去水盤<br />
                    - 檢查電壓值及電流值<br />
                    - 檢查機組及機件運作<br />
                    - 檢查有否不正常噪音及震動<br />
                    - 記錄任何冷氣系統失效及維護事故<br />
                    - 清洗完一次冷氣維護及報告<br />
                    - 每年補入足夠雪種一次<br />
                    <br />
                    合約期：由20/6/2015 至 19/6/2016
                    </td>
                    <td align="center">1</td>
                    <td align="center">HK$15,120</td>
                    <td align="right">HK$15,120</td>
                </tr>
                <tr>
                    <td>第二年續約優惠:</td>
                    <td align="center">1</td>
                    <td align="center">-HK$2,760</td>
                    <td align="right">-HK$2,760</td>
                </tr>
                <tr>
                    <td class="highlight" align="right" colspan="3">Total 總價</td>
                    <td class="highlight" align="right">HK$19,920</td>
                </tr>
                <tr>
                    <td class="highlight" align="right" colspan="3">分 12 期，每期費用</td>
                    <td class="highlight" align="right">HK$1,660</td>
                </tr>
        </table>
        
        <p class="subtitle">Payment Term 付款條款</p>
        <p>- 付款方式：每月5號付款，共分 12 次<br />
        - 工作時間：星期一至星期六，09:00 至 18:00（公眾假期除外）<br />
        </p>
        <p class="subtitle">Remark 備註</p>
        <p>服務事項包括：<br />
        本公司承諾以最優惠價格提供一切維修及零件服務<br />
        本公司承諾優先安排客戶的檢查服務<br />
        包括勞工保險及一千萬第三者保險<br />
        Our Qualification:<br />
        - 政府註冊冷氣工程承辦商／註冊機電工程處電業承辦商<br />
        - II 級及 III 級類別小型工程承辦商／電工 A 牌技工<br />
        - 環境保護署室內空氣質素服務供應商<br />
        </p>
EOF;

        // output the HTML content
        $pdf->writeHTML($html, true, false, true, false, '');

        // ---------------------------------------------------------
        $pdf->Cell('','','Franky Fok',0,1,'L',false,'',0,false,'T','M');
        $pdf->Cell('','','Talent Engineering HK Ltd.',0,0,'L',false,'',0,false,'T','M');
        $pdf->Cell('','','Confirmed By:_______________________________________',0,1,'R',false,'',0,false,'T','M');
        $pdf->Cell('','','地址：觀塘開源道47號凱源工業大廈14樓F室',0,1,'L',false,'',0,false,'T','M');
        $pdf->Cell('','','Address: Rm F, 14/F., High Win Factory Building, 47 Hoi Yuen Road, Kwun Tong, Kowloon',0,1,'L',false,'',0,false,'T','M');
        //Close and output PDF document
        $pdf->Output('invoice.pdf', 'I');

        //============================================================+
        // END OF FILE
        //============================================================+
    }
}
